# GB-CI-CD

### Task
Создать страницу, которая будет отдаваться при переходе на несуществующую страницу из GitLab Pages. То есть, если я сейчас у себя в репозитории перейду на страницу http://nikolai.mishchenkov.gitlab.io/lession-one/sdfsdfdsf (sdfsdfdsf - несуществующая страница), то получу стандартную гитлабовскую отбивку о том, что такой страницы не существует (код 404). Нужно сделать свою 404 страницу и прислать скриншот. Свою страницу (404.html) можно сделать любой. [Подсказка](https://docs.gitlab.com/ee/user/project/pages/introduction.html#custom-error-codes-pages). Самый простой [пример HTML-страницы](https://www.w3schools.com/html/html_basic.asp) . 
